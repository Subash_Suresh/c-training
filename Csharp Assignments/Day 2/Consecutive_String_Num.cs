﻿using System;
using System.Collections.Generic;
using System.Text;
using Day1;

namespace Day_2
{   /// <summary>
    ///  deals with concetive number checks if the inputs are in string format 
    /// </summary>
    class Concesutive_String_Num
    {   /// <summary>
        /// Checks if the input is concecutive or not 
        /// </summary>
        public void ConsecutiveStringCheck()
        {
            Console.WriteLine("Enter Five numbers seperated by '-' eg: '5-4-2-3-1' ");
            string numString = Console.ReadLine();
            string[] numStringarr = numString.Split('-');
            int[] numArray = new int[5];
            for (int i = 0; i < 5; i++)
            {
                    numArray[i] = Convert.ToInt32(numStringarr[i]);
                    Console.WriteLine(numArray[i]);
            }
            ConsecutiveCheck ins1 = new ConsecutiveCheck();
            ins1.ConcecutiveChecker(numArray);
        }
    }
}
