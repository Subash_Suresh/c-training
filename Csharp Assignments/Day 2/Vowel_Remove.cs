﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Day_2
{   /// <summary>
    ///Removes all the vowels from a given string 
    /// </summary>
    class Vowel_Remove
    {
        public void VowelRemover()
        {
            Console.WriteLine("Enter the number of string you would like to enter");
            int n = Convert.ToInt32(Console.ReadLine());
            string[] words = new string[n];
            string[] vowels = { "a", "e", "i", "o", "u"};
            Console.WriteLine("Enter the words:");
            //checks and replaces vowels with an empty string
            for(int i=0;i<n;i++)
            {
                words[i] = Console.ReadLine();
                for(int j=0;j<5;j++)
                {
                    words[i] = words[i].Replace(vowels[j],string.Empty);
                }
            }
            Console.WriteLine("The vowel removed words are:");
            foreach(string x in words)
            {
                Console.WriteLine(x);
            }
        }
    }
}
