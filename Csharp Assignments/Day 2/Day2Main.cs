﻿using System;
/// <summary>
/// This contains all the classes related to the Day 2 Assignments
/// </summary>

namespace Day_2
{
    class Day2Main
    {
        static void Main(string[] args)
        {
            Concesutive_String_Num csn = new Concesutive_String_Num();
            Vowel_Remove vr = new Vowel_Remove();
            Vowel_Count vc = new Vowel_Count();
            SwapNum sn = new SwapNum();
            Console.WriteLine("Day 2 Assignments:/n1.Checking concecutive numbers in string format");
            Console.WriteLine("2.Removing vowels from n given words");
            Console.WriteLine("3.Counting the number of vowels in a given string using swith case and enum");
            Console.WriteLine("4.Swapping 2 given numbers ");
            int choice = Convert.ToInt32(Console.ReadLine());
            bool repeat = true;
            while (repeat)
            {
                switch (choice)
                {
                    case (1):
                        Console.WriteLine("******Checking concecutive numbers in string format******");
                        csn.ConsecutiveStringCheck();
                        break;
                    case (2):
                        Console.WriteLine("******Removing vowels from n given words******");
                        vr.VowelRemover();
                        break;
                    case (3):
                        Console.WriteLine("******Counting the number of vowels in a given string using swith case and enum******");
                        vc.VowelCounter();
                        break; ;
                    case (4):
                        Console.WriteLine("******Swapping 2 given numbers******");
                        break;
                    default:
                        Console.WriteLine("Wrong input");
                        break;
                }
                Console.WriteLine("Do you want to try again?(Y/N)");
                repeat = Console.ReadLine().ToUpper() == "Y" ? true : false;
            }

        }
    }
}
