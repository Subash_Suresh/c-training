﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_2
{   /// <summary>
    ///Swaps the two given numbers 
    /// </summary>
    class SwapNum
    {
        public void SwapNumbers()
        {
            Console.WriteLine("Enter Number1:");
            int num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter Number2:");
            int num2 = Convert.ToInt32(Console.ReadLine());
            (num1, num2) = (num2, num1);
            Console.WriteLine("the given numbers were swapped num1:{0}\n num2:{0}",num1,num2);
        }
    }
}
