﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_2
{   /// <summary>
    ///Counts the number of vowels in a given string 
    /// </summary>
    class Vowel_Count
    {
        public enum vowel {a,e,i,o,u};
        public void VowelCounter()
        {
            Console.WriteLine("Please enter a string to check vowel count");
            char[] checkPhrase = Console.ReadLine().ToLower().ToCharArray();
            int count = 0;
            foreach(vowel letter in checkPhrase)
            {   //if any of the vowels occur then count is incremented
                switch(letter)
                {
                    case vowel.a:
                    case vowel.e:
                    case vowel.i:
                    case vowel.o:
                    case vowel.u:
                        count++;
                        break;
                }
            }
        }
    }
}
