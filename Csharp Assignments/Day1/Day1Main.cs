﻿using System;

namespace Day1
{/// <summary>
 ///DayMain class is used to call all other classes in Day1 for execution
 /// </summary>
 //some comments here are  done in xml format in the respective classes as a part of  Question 5
    class Day1Main
    {
        static void Main(string[] args)
        {   //Creating a new instance for the classes that are going to called.
            Hello_World HW = new Hello_World();
            Area_of_Figures AoF = new Area_of_Figures();
            ConsecutiveCheck CC = new ConsecutiveCheck();
            PascalTriangle PT = new PascalTriangle();
            bool repeat = true;
            while (repeat)
            {
                Console.WriteLine("Please select the program from the menu for excecution");
                Console.WriteLine("1.Hello World\n2.Area of Figures\n3.Consecutive number checker\n" +
                                  "4.Pascal Triangle generator");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case (1):
                        Console.WriteLine("\n\n********* Hello World Program *********\n");
                        HW.HelloWorld();
                        break;
                    case (2):
                        Console.WriteLine("\n\n********* Area of figures Program *********\n");
                        AoF.Area();
                        break;
                    case (3):
                        Console.WriteLine("\n\n********* Concecutive Number Check Program *********\n");
                        int [] numArray = new int[5];
                        for (int i = 0; i < 5; i++)
                        {
                            Console.WriteLine("Enter number {0}:", i + 1);
                            numArray[i] = Convert.ToInt32(Console.ReadLine());
                        }
                        CC.ConcecutiveChecker(numArray);
                        break;
                    case (4):
                        Console.WriteLine("\n\n********* Pascal's Triangle Program *********\n");
                        PT.PascalTriangleGen();
                        break;
                    default:
                        Console.WriteLine("Wrong Input");
                        break;
                }
                Console.WriteLine("Do you want to try again?(Y/N)");
                repeat = Console.ReadLine().ToUpper() == "Y" ? true : false;
            }
            
        }
    }
}
