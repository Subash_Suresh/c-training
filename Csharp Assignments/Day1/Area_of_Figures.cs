﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day1
{/// <summary>
/// This is Class contains Question 2 of Day1 assignment
/// </summary>
    class Area_of_Figures
    {
        const float pi = 3.14f;
        /// <summary>
        /// This is used to find area of rectangle
        /// </summary>
        /// <returns></returns>
        double Rectangle()
        {
            double length, breath;
            Console.WriteLine("Enter Length of the Rectangle: ");
            length = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter Breath of the Rectangle: ");
            breath = Convert.ToDouble(Console.ReadLine());
            return length * breath;
        }
        /// <summary>
        /// This is used to find area of circle
        /// </summary>
        /// <returns></returns>
        double Circle()
        {
            Console.WriteLine("Enter Radius: ");
            double radius = Convert.ToDouble(Console.ReadLine());
            return pi * radius * radius;
        }
        /// <summary>
        /// This is used to find area of Sqaure
        /// </summary>
        /// <returns></returns>
        double Square()
        {
            Console.WriteLine("Enter the side of a square: ");
            double side = Convert.ToDouble(Console.ReadLine());
            return side * side;
        }
        /// <summary>
        /// This method is used for getting input and producing the area output to the user
        /// </summary>
        public void Area()
        {
            double area = 0;
            Area_of_Figures aof = new Area_of_Figures();
                Console.WriteLine("Please select the figure by entering the  coresponding number\n" +
                                    "1.Rectangle\n2.Circle\n3.Square");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case (1):
                        area = aof.Rectangle();
                        break;
                    case (2):
                        area = aof.Circle();
                        break;
                    case (3):
                        area = aof.Square();
                        break;
                    default:
                        Console.WriteLine("You have entered an invalid input");
                        break;
                }
                Console.WriteLine("Area= {0}", area);
        }
    }
}
