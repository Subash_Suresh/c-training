﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day1
{/// <summary>
/// This is question 4 of day 1 assignments
/// </summary>
    class PascalTriangle
    {/// <summary>
    /// generates pascal's triangle for the given order
    /// </summary>
        public void PascalTriangleGen()
        {
            int order, i, k;
            Console.WriteLine("Enter the order of the pascal triangle");
            order = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n");
            int spacing = order;
            for (i = 0; i < order; i++)
            {   for (int j = 0; j < spacing; j++)
                {
                    Console.Write(" ");
                }
                spacing--;
                int num = 1;
                for (k = 0; k <= i; k++)
                {
                    Console.Write("{0} ",num);
                    num = (num * (i - k) / (k + 1));
                }
                Console.WriteLine("");
            }
        }
    }
}
