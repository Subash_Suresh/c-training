﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day1
{/// <summary>
/// This class contains Question 1 of day 1 assignments
/// </summary>
    class Hello_World
    {/// <summary>
    /// This prints "Hello World"
    /// </summary>
        public void HelloWorld()
        {
            Console.WriteLine("Hello World!");
        }
    }
}
