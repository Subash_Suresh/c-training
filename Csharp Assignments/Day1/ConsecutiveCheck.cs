﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Day1
{/// <summary>
/// This class contains question 3 of day 1 assignment 
/// </summary>
    public class ConsecutiveCheck
    {
        /// <summary>
        /// used to check if the entered set of 5 numbers is concecutive
        /// </summary>
        public void ConcecutiveChecker(int[] numArray)
        {
            int concecutiveCheckFlag = 0;
            for (int i = 0; i < 5; i++)
            {
                if (i < 4)
                {
                    if (numArray[i + 1] != numArray[i] + 1)
                    {
                        concecutiveCheckFlag = 1;
                    }
                }
            }
            if (concecutiveCheckFlag == 0)
            {
                Console.WriteLine("They are Consecutive numbers!!!\n");
            }
            else
            {
                Console.WriteLine("They are not concecutive numbers :(\n");
            }
        }
    }
}
