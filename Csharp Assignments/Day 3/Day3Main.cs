﻿using System;
/// <summary>
/// This Contains Classes related to Day 3 assignments regarding complex numbers
/// </summary>
namespace Day_3
{   
    class Day3Main
        /// <summary>
        /// Takes the complex number input in form of a string
        /// </summary>
    {   public void ValueInput(ComplexNumber ins)
        {
            Console.WriteLine("Complex Number Format: a+ib\nEnter a:");
            ins.A = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Enter b:");
            ins.B = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Entered number is :"+ins.ToString());

        }
        static void Main(string[] args)
        {
            Day3Main insmain = new Day3Main();
            ComplexNumber ins1 = new ComplexNumber();
            Console.WriteLine("What do you wnt to do with the complex number\n1.Do you want to enter new values");
            Console.WriteLine("2.Find the Magnitude of the Complex number");
            Console.WriteLine("3.Find Addition of two complex numbers");
            int choice = Convert.ToInt32(Console.ReadLine());
            //Processing the user's choice 
            switch(choice)
            {
                case (1):
                    insmain.ValueInput(ins1);
                    break;
                case (2):
                    double Mag=ins1.GetMagnitude();
                    Console.WriteLine("The magnitude of the complex number is {0}:",Mag);
                    break;
                case (3):
                    Console.WriteLine("Complex Number Format: a+ib/nEnter number1:");
                    string num1 = Console.ReadLine();
                    Console.WriteLine("Enter number2:");
                    string num2 =Console.ReadLine();
                    string result = ins1.AddComplexNum(num1, num2);
                    Console.WriteLine("The Result is :"+result);
                    break;
                default:
                    Console.WriteLine("Wrong Input");
                    break;
            }
        }
    }
}
