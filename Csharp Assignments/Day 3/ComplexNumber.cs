﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_3
{
    class ComplexNumber
    {
        private double a, b;
        /// <summary>
        ///This property is used to set values a in a+ib
        /// </summary>
        public double A
        {
            get
            {
                return a;
            }
            set
            {
                a= value;
            }
        }
        /// <summary>
        ///This property is used to set values b in a+ib
        /// </summary>
        public double B
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
            }
        }
        //Contructor
        public ComplexNumber()
        {
            Console.WriteLine("The Default number is: 2-i3\n");
            double a = 2;
            double b = -3;
        }
        //To return a string in a+ib format
        /// <summary>
        /// Returns the number as a+ib in string format
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return "("+Convert.ToString(a)+")" + "+i" +"(" +Convert.ToString(b)+")";
        }
        /// <summary>
        /// Returns the magnitude of the complex numbers
        /// </summary>
        /// <returns></returns>
        public double GetMagnitude()
        {
            return Math.Pow((Math.Pow(a, 2) + Math.Pow(b, 2)), 0.5f);
        }
        /// <summary>
        /// This fuction adds two complex numbers returns the value in form of a string 
        /// </summary>
        /// <param name="CompNum1"></Complex Number 1>
        /// <param name="CompNum2"></Complex number 2>
        /// <returns></returns>
        public string AddComplexNum(string CompNum1,string CompNum2)
        {
            ComplexNumber ins = new ComplexNumber();
            //The CompNumChar array consists of {a,+,i,b}
            double real1 = Convert.ToDouble(CompNum1.Substring(0,CompNum1.IndexOf('+')));
            double imaginary1 = Convert.ToDouble(CompNum1.Substring(CompNum1.IndexOf('i')+1));
            //The CompNumChar array consists of {a,+,i,b}
            double real2 = Convert.ToDouble(CompNum2.Substring(0, CompNum2.IndexOf('+')));
            double imaginary2 = Convert.ToDouble(CompNum2.Substring(CompNum2.IndexOf('i')+1));
            ins.A = real1 + real2;
            ins.B = imaginary1 + imaginary2;
            return ins.ToString();            
        }

    }
}
