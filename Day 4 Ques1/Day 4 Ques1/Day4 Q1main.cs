﻿using System;

namespace Day_4_Ques1
{
    class Sorting
    {
        static void Main(string[] args)
        {   BubbleSort bs = new BubbleSort();
            SelectonSort ss = new SelectonSort();
            int max, middle, min;
            Console.WriteLine("Enter the Length of the array");
            int len = Convert.ToInt32(Console.ReadLine());
            int[] arr = new int[len];
            Console.WriteLine("Enter Array elements for sorting");
            for (int i=0;i<len;i++)
            {
                arr[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Select sorting method:\n1.Bubble Sort\n2.Selection Sort");
            if(Convert.ToInt32(Console.ReadLine())==1)
            {
                arr = bs.Sort(arr);
                max = bs.arrMax(arr);
                min = bs.arrMin(arr);
                middle = bs.arrMid(arr);
            }
            else
            {
                arr = ss.Sort(arr);
                max = ss.arrMax(arr);
                min = ss.arrMin(arr);
                middle = ss.arrMid(arr);
            }
            Console.WriteLine("Sorted Array");
            for(int i=0;i<len;i++)
            {
                Console.Write("{0} ",arr[i]);
            }
            Console.WriteLine("");
            Console.WriteLine("Max:{0}\nMin:{1}\nMid:{2}",max,min,middle);
        }
    }
}
