﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_4_Ques1
{   public interface ISort
    {
        int[] Sort(int[] arr);
        int arrMax(int[] arr);
        int arrMin(int[] arr);
        int arrMid(int[] arr);
    }

    public class BubbleSort : ISort
    {
        //Finiding maximum
        public int arrMax(int[] arr)
        {
            int[] sortedArray = Sort(arr);
            return sortedArray[sortedArray.Length - 1];
        }
        //Finding minimum
        public int arrMin(int[] arr)
        {
            int[] sortedArray = Sort(arr);
            return sortedArray[0];
        }
        //finding middle term
        public int arrMid(int[] arr)
        {
            int[] sortedArray = Sort(arr);
            return sortedArray[(sortedArray.Length / 2)];
        }

        public int[] Sort(int[] arr)
        {
            //This is Bubblesort
            int n = arr.Length;
            for (int i = 0; i < n - 1; i++)
                for (int j = 0; j < n - i - 1; j++)
                    if (arr[j] > arr[j + 1])
                    {
                        // swap temp and arr[i] 
                        int temp = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = temp;
                    }
            return arr;
        }
    }

    public class SelectonSort:BubbleSort,ISort
    {
        public new int[] Sort(int[] arr)
        {
            //This is Selection  Sort
            int n = arr.Length;

            // One by one move boundary of unsorted subarray 
            for (int i = 0; i < n - 1; i++)
            {
                // Find the minimum element in unsorted array 
                int min_index = i;
                for (int j = i + 1; j < n; j++)
                    if (arr[j] < arr[min_index])
                        min_index = j;

                // Swap the found minimum element with the first element
                int temp = arr[min_index];
                arr[min_index] = arr[i];
                arr[i] = temp;
            }
            return arr;
        }

    }
}
