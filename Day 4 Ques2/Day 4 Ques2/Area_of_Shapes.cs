﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Day_4_Ques2
{
    abstract class Shapes
    {
        protected const double pi = 3.14;
        /// <summary>
        /// Finds area of the shape under consideration
        /// </summary>
        /// <returns></returns>
        public abstract double area();
    }
    class Rectangle:Shapes
    {
        public double length;
        public double breath;
        /// <summary>
        /// gets rectangle inputs
        /// </summary>
        /// <param name="l"></param>
        /// <param name="b"></param>
        public void input(double l=0,double b=0)
        {
            length = l;
            breath = b;
        }

        public override double area()
        {
            return length * breath;
        }
    }
    class Triangle : Shapes
    {
        public double triangleBase;
        public double height;
        /// <summary>
        /// gets Triangle inputs
        /// </summary>
        /// <param name="b"></param>
        /// <param name="h"></param>
        public void input(double b = 0, double h = 0)
        {
            triangleBase = b;
            height = h;
        }
        public override double area()
        {
            return triangleBase*height*0.5;
        }
    }
    class Square : Shapes
    {
        public double side;
        /// <summary>
        ///gets Square inputs
        /// </summary>
        /// <param name="s"></param>
        public void input(double s=0)
        {
            side = s;
        }
        public override double area()
        {
            return side * side;
        }
    }
    class Circle : Shapes
    {
        public double radius;
        /// <summary>
        /// gets Circle inputs
        /// </summary>
        /// <param name="r"></param>
        public void input(double r = 0)
        {
            radius = r;
        }
        public override double area()
        {
            return pi * radius * radius;
        }
    }
}
