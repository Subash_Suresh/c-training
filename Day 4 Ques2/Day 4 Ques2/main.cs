﻿using System;

namespace Day_4_Ques2
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle r = new Rectangle();
            Triangle t = new Triangle();
            Square s = new Square();
            Circle c = new Circle();
            double area;
            Console.WriteLine("Area finder\nSelect the shape you want find the area for");
            Console.WriteLine("1.Rectangle\n2.Triangle\n3.Square\n4.Circle");
            int choice = Convert.ToInt32(Console.ReadLine());
            switch(choice)
            {
                case (1):
                    Console.WriteLine("Enter length of the Rectangle");
                    double length = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter breath of the Rectangle");
                    double breadth = Convert.ToDouble(Console.ReadLine());
                    r.input(length, breadth);
                    area = r.area();
                    Console.WriteLine("Area of the Rectangle:{0}",area);
                    break;
                case (2):
                    Console.WriteLine("Enter base of the Triangle");
                    double triangleBase = Convert.ToDouble(Console.ReadLine());
                    Console.WriteLine("Enter height of the Triangle");
                    double height = Convert.ToDouble(Console.ReadLine());
                    t.input(triangleBase, height);
                    area = t.area();
                    Console.WriteLine("Area of the triangle:{0}", area);
                    break;
                case (3):
                    Console.WriteLine("Enter side  of the square");
                    double side = Convert.ToDouble(Console.ReadLine());
                    s.input(side);
                    area = s.area();
                    Console.WriteLine("Area of the Square:{0}", area);
                    break;
                case (4):
                    Console.WriteLine("Enter radius of the Circle");
                    double radius= Convert.ToDouble(Console.ReadLine());
                    c.input(radius);
                    area = c.area();
                    Console.WriteLine("Area of the Circle:{0}", area);
                    break;
                default:
                    Console.WriteLine("Wrong Input");
                    break;
            }
        }

       
    }
}
